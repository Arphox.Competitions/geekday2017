﻿namespace GameLogic.DataProcessor
{
    public abstract class UdpBlock
    {
        public int MatchId { get; }

        public UdpBlock(int matchId)
        {
            MatchId = matchId;
        }
    }
}