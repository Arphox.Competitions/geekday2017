﻿using GameLogic.AiModel;
using GameLogic.DataProcessor;
using GameLogic.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace GameLogic
{
    internal sealed class ArphoxAI
    {
        private static readonly Random _random = new Random();
        private int _matchId;
        private int _roundCounter = 0;
        private Dictionary<int, Func<int, string>> predefinedMoves = new Dictionary<int, Func<int, string>>();

        internal MatchDataUdpBlock PreviousMatchDataUdpBlock { get; private set; }
        internal MatchDataUdpBlock CurrentMatchDataUdpBlock { get; private set; }

        internal MatchStatisticsUdpBlock PreviousMatchStatisticsUdpBlock { get; private set; }
        internal MatchStatisticsUdpBlock CurrentMatchStatisticsUdpBlock { get; private set; }

        internal void AcceptData(MatchDataUdpBlock data)
        {
            PreviousMatchDataUdpBlock = CurrentMatchDataUdpBlock;
            CurrentMatchDataUdpBlock = data;
        }
        internal void AcceptStat(MatchStatisticsUdpBlock stat)
        {
            PreviousMatchStatisticsUdpBlock = CurrentMatchStatisticsUdpBlock;
            CurrentMatchStatisticsUdpBlock = stat;
            if (MatchStatisticsUdpBlock.IsGoalScored(PreviousMatchStatisticsUdpBlock, CurrentMatchStatisticsUdpBlock))
            {
                Console.WriteLine("GOAL! round counter reset to 0.");
                _roundCounter = 0;
            }
        }

        internal MatchData MatchData { get; set; }

        internal ArphoxAI(int matchId)
        {
            _matchId = matchId;
            predefinedMoves.Add(1, GetPredefinedResponse1);
            predefinedMoves.Add(2, GetPredefinedResponse2);
        }

        internal string CreateResponse(int player)
        {
            Thread.Sleep(50);
            _roundCounter++;
            if (CurrentMatchDataUdpBlock == null)
                return GetPredefinedResponse1(player);
            else
                MatchData = new MatchData(CurrentMatchDataUdpBlock, player);

            // if (predefinedMoves.ContainsKey(_roundCounter)) return predefinedMoves[_roundCounter](player);

            DoLogic();

            NormalizeResponses(MatchData.MyUnits);
            return ConvertResponsesToString(MatchData.MyUnits);
        }

        private void DoLogic()
        {
            List<PointD> directionVectorsTowardsBall = MatchData.MyUnits.Select(u => u.CalculateDirectionVector(MatchData.BallPosition)).ToList();

            RefineDirections(MatchData.MyUnits, directionVectorsTowardsBall);

            HomeComingIfNeed(directionVectorsTowardsBall);
            directionVectorsTowardsBall.CopyValuesToOutputsOf(MatchData.MyUnits);
        }

        private void HomeComingIfNeed(List<PointD> directionVectorsTowardsBall)
        {
            foreach (PointD p in directionVectorsTowardsBall)
            {
                if (!MatchData.IsTowardsEnemy(p))
                {
                    p.Y = 0;
                    p.X = MatchData.BoostX(p.X);
                }
            }
        }

        private void RefineDirections(List<PointD> myUnits, List<PointD> directionVectorsTowardsBall)
        {
            return;

            const double correctionFactor = 0.1;

            for (int i = 0; i < directionVectorsTowardsBall.Count; i++)
            {
                PointD unit = myUnits[i];
                PointD direction = directionVectorsTowardsBall[i];
                PointD ball = MatchData.BallPosition;
                double xDistance = Math.Abs(ball.X - unit.X);

                double y = AxMath.GetYByX(ball.X, ball.Y, unit.X, unit.Y, MatchData.GetMyBaseLine());
                if (AxMath.IsGate(y))
                    continue;
                else
                {
                    if (AxMath.IsUnderGate(y))
                    {   // If the y is too low (over the gate), we want to increase it by decreasing the direction vector's Y component
                        direction.Y *= (1 - correctionFactor);
                    }
                    else if (AxMath.IsOverGate(y))
                    {   // If the y is too high (under the gate), we want to decrease it by increasing the direction vector's Y component
                        direction.Y *= (1 + correctionFactor);
                    }
                }
            }
        }

        private string GetPredefinedResponse1(int player)
        {
            int ballAngleNormal = 100;
            int ballAngle = 20;
            int sideAngle = 25;

            if (player == 1)
            {
                PointD p1 = new PointD(0, 0) { OutputX = ballAngleNormal, OutputY = ballAngle }; p1.NormalizeOutputs();
                PointD p2 = new PointD(0, 0) { OutputX = ballAngleNormal, OutputY = sideAngle }; p2.NormalizeOutputs();
                PointD p3 = new PointD(0, 0) { OutputX = ballAngleNormal, OutputY = -sideAngle }; p3.NormalizeOutputs();
                List<PointD> responses = new List<PointD>() { p1, p2, p3 };
                return ConvertResponsesToString(responses);
            }
            else if (player == 2)
            {
                PointD p1 = new PointD(0, 0) { OutputX = -ballAngleNormal, OutputY = -ballAngle }; p1.NormalizeOutputs();
                PointD p2 = new PointD(0, 0) { OutputX = -ballAngleNormal, OutputY = -sideAngle }; p2.NormalizeOutputs();
                PointD p3 = new PointD(0, 0) { OutputX = -ballAngleNormal, OutputY = sideAngle }; p3.NormalizeOutputs();
                List<PointD> responses = new List<PointD>() { p1, p2, p3 };
                return ConvertResponsesToString(responses);
            }
            throw new InvalidOperationException();
        }

        private string GetPredefinedResponse2(int player)
        {
            List<PointD> directionVectorsTowardsBall = MatchData.MyUnits.Select(u => u.CalculateDirectionVector(MatchData.BallPosition)).ToList();
            PointD directionVectorToBall = directionVectorsTowardsBall[1];
            MatchData.MyUnits[1].OutputX = directionVectorToBall.X;
            MatchData.MyUnits[1].OutputY = directionVectorToBall.Y;

            PointD p1 = new PointD(0, 0) { OutputX = directionVectorsTowardsBall[0].OutputX, OutputY = directionVectorsTowardsBall[0].OutputY };
            p1.NormalizeOutputs(GameSettings.MaxSpeed * 2);
            PointD p2 = new PointD(0, 0) { OutputX = MatchData.MyUnits[1].OutputX, OutputY = MatchData.MyUnits[1].OutputY };
            p2.NormalizeOutputs();
            PointD p3 = new PointD(0, 0) { OutputX = directionVectorsTowardsBall[2].OutputX, OutputY = directionVectorsTowardsBall[2].OutputY };
            p3.OutputX *= 1.5;
            p3.NormalizeOutputs();
            List<PointD> responses = new List<PointD>() { p1, p2, p3 };

            if (player == 2)
            {
                p2.OutputX *= -1;
                p2.OutputY *= -1;
            }
            return ConvertResponsesToString(responses);
        }







        private static List<PointD> InitResponses()
        {
            List<PointD> responses = new List<PointD>();
            for (int i = 0; i < 3; i++)
                responses.Add(new PointD(0, 0));
            return responses;
        }
        private string ConvertResponsesToString(List<PointD> responses)
        {
            List<double> resp = new List<double>(responses.Count * 2);
            foreach (var item in responses)
            {
                resp.Add(item.OutputX);
                resp.Add(item.OutputY);
            }
            string x = string.Join("\n", resp);
            x = x.Replace(".", ",");
            return x;
        }
        private static void NormalizeResponses(List<PointD> myUnits) => myUnits.ForEach(r => r.NormalizeOutputs(GameSettings.MaxSpeed));
    }
}