﻿using GameLogic.DataProcessor;
using System.Collections.Generic;

namespace GameLogic.AiModel
{
    internal sealed class MatchData
    {
        public int Player { get; }
        public int MatchId { get; }

        public PointD BallPosition { get; private set; }

        public List<PointD> MyUnits { get; private set; }
        public List<PointD> EnemyUnits { get; private set; }

        public bool IsTowardsEnemy(PointD directionVector)
        {
            if (Player == 1)
            {
                return directionVector.X > 0;
            }
            else
            {
                return directionVector.X < 0;
            }
        }
        public double GetMyBaseLine()
        {
            if (Player == 1)
                return 0;
            else
                return 1000;
        }
        public double BoostX(double x)
        {
            if (x < 0)
                x = -10000;
            else if (x > 0)
                x = 10000;

            return x;
        }



        public MatchData(MatchDataUdpBlock matchDataUdpBlock, int player)
        {
            Player = player;
            MatchId = matchDataUdpBlock.MatchId;

            BallPosition = matchDataUdpBlock.BallPosition;

            MyUnits = new List<PointD>()
            {
                matchDataUdpBlock.Player1Unit1Position,
                matchDataUdpBlock.Player1Unit2Position,
                matchDataUdpBlock.Player1Unit3Position
            };
            EnemyUnits = new List<PointD>()
            {
                matchDataUdpBlock.Player2Unit1Position,
                matchDataUdpBlock.Player2Unit2Position,
                matchDataUdpBlock.Player2Unit3Position
            };

            if (player == 2)
            {   // Swap dem
                var temp = MyUnits;
                MyUnits = EnemyUnits;
                EnemyUnits = temp;
            }
        }
    }
}