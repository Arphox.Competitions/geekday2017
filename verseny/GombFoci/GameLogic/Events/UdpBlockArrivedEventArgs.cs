﻿using GameLogic.DataProcessor;
using System;

namespace GameLogic.Events
{
    public class UdpBlockArrivedEventArgs : EventArgs
    {
        public int MatchId { get; set; }
        public MatchDataUdpBlock MatchDataUdpBlock { get; set; }

        public UdpBlockArrivedEventArgs(int matchId, MatchDataUdpBlock matchDataUdpBlock)
        {
            MatchId = matchId;
            MatchDataUdpBlock = matchDataUdpBlock;
        }
    }
}