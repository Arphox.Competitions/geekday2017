﻿using System;
using System.Linq;

namespace GameLogic.Tools
{
    internal static class RandomResponseGenerator
    {
        private static readonly Random _random = new Random();

        internal static string Generate()
        {
            double[] numbers = new double[6];
            for (int i = 0; i < 6; i++)
            {
                //numbers[i] = Math.Round(_random.NextDouble(), 6) * 100;
                if (_random.NextDouble() < 0.5)
                    numbers[i] *= -1;
            }
            return string.Join("\n", numbers.Select(n => n.ToString().Replace(".", ",")));
        }
    }
}