﻿using System;
using System.Collections.Generic;

namespace GameLogic.Tools
{
    internal static class UdpDataFragmenter
    {
        internal static List<byte[]> FragmentUdpData(byte[] data)
        {
            List<byte[]> blocks = new List<byte[]>();
            int a = 0;
            int b = 0;
            for (int i = 0; i < data.Length; i++)
            {
                if (i < data.Length - 4 && data[i] == 255 && data[i + 1] == 255 && data[i + 2] == 255 && data[i + 3] == 255)
                { // 4 * 0xFF
                    b = i - 1;
                    if (b > 0)
                    {
                        blocks.Add(SubArray(data, a + 4, b - a + 1 - 4));
                        a = b + 1;
                    }
                }
            }

            return blocks;
        }

        internal static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }
    }
}