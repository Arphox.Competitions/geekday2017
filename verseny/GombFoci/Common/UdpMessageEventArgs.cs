﻿using System;

namespace Common
{
    public sealed class UdpMessageEventArgs : EventArgs
    {
        public byte[] Data { get; }
        public string RemoteIPAddress { get; }

        public UdpMessageEventArgs(byte[] data, string remoteIPAddress)
        {
            Data = data;
            RemoteIPAddress = remoteIPAddress;
        }
    }
}