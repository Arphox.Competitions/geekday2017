﻿using ArphoxServer.HTTP;
using ArphoxServer.UDP;
using GameLogic;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;

namespace ArphoxServer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly GameController _gameController = new GameController();
        private readonly MyVision myVisionWindow = new MyVision();
        private string LogFileName = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ".txt";

        public ObservableCollection<string> Log { get; } = new ObservableCollection<string>();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            myVisionWindow.Show();
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            _gameController.MessageLogger += LogMessage;
            _gameController.UdpBlockArrived += myVisionWindow.Draw;

            ArphoxWebServer webServer = new ArphoxWebServer(_gameController);
            webServer.EntryLogged += LogMessage;
            webServer.RunInBackground();

            ArphoxUdpServer udpServer = new ArphoxUdpServer(_gameController);
            udpServer.EntryLogged += LogMessage;
            udpServer.RunInBackground();
        }

        private void LogMessage(object sender, string msg)
        {
            Dispatcher.Invoke(() =>
            {
                string logMsg = $"{Now}: {msg}";
                Log.Insert(0, logMsg);
                File.AppendAllText(LogFileName, logMsg + Environment.NewLine);
            });
        }

        private static string Now => DateTime.Now.ToString("HH:mm:ss");
    }
}
