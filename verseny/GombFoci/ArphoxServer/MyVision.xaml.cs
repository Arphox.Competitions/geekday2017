﻿using GameLogic.DataProcessor;
using GameLogic.Events;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace ArphoxServer
{
    /// <summary>
    /// Interaction logic for MyVision.xaml
    /// </summary>
    public partial class MyVision : Window
    {
        public MyVision()
        {
            InitializeComponent();
        }

        public void Draw(object sender, UdpBlockArrivedEventArgs args)
        {
            if (args.MatchId != ServerSettings.MatchIdToWatch)
                return;

            MatchDataUdpBlock matchData = args.MatchDataUdpBlock;

            Dispatcher.Invoke(() =>
            {
                canvas.Children.Clear();
                DrawGates();

                DrawToCanvas(matchData.BallPosition, 30, Brushes.Black);

                DrawToCanvas(matchData.Player1Unit1Position, 60, Brushes.Red);
                DrawToCanvas(matchData.Player1Unit2Position, 60, Brushes.Red);
                DrawToCanvas(matchData.Player1Unit3Position, 60, Brushes.Red);

                DrawToCanvas(matchData.Player2Unit1Position, 60, Brushes.Blue);
                DrawToCanvas(matchData.Player2Unit2Position, 60, Brushes.Blue);
                DrawToCanvas(matchData.Player2Unit3Position, 60, Brushes.Blue);
            });
        }

        private void DrawToCanvas(PointD point, int size, Brush color)
        {
            Ellipse e = new Ellipse();
            e.Height = size;
            e.Width = size;
            e.Fill = color;
            e.Stroke = color;
            Canvas.SetLeft(e, point.X - size / 2);
            Canvas.SetTop(e, point.Y - size / 2);
            canvas.Children.Add(e);
        }

        private void DrawGates()
        {
            Rectangle gate1 = new Rectangle();
            gate1.Height = 175;
            gate1.Width = 10;
            gate1.Fill = Brushes.Black;
            gate1.Stroke = Brushes.Black;
            Canvas.SetLeft(gate1, 0);
            Canvas.SetTop(gate1, 262.5);
            canvas.Children.Add(gate1);

            Rectangle gate2 = new Rectangle();
            gate2.Height = 175;
            gate2.Width = 10;
            gate2.Fill = Brushes.Black;
            gate2.Stroke = Brushes.Black;
            Canvas.SetLeft(gate2, 990);
            Canvas.SetTop(gate2, 262.5);
            canvas.Children.Add(gate2);
        }
    }
}