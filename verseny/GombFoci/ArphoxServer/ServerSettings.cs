﻿using System.Collections.Generic;

namespace ArphoxServer
{
    public static class ServerSettings
    {
        public const int UdpPort = 41261;
        public const string HttpUrl = "http://192.168.1.44:8080/process/";
        public static readonly List<string> WhiteListedIps = new List<string>() { "192.168.1.126", "192.168.1.44" };
        public const int MatchIdToWatch = 58;

        // image Base64:
        //iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAIAAAAlC+aJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABqSURBVGhD7c9RDYAwFMDACeGLIGdmJwwNIOPykjY1cOu9ntEH0AfQB9AH0AfQB9AH0AfQB9AH0K9veAF0AXQBdAF0AXQBdAF0AXQBdAF08wH32aMPoA+gD6APoA+gD6APoA+gD6APYD/7By7xgCULQrZjAAAAAElFTkSuQmCC
    }
}