Sorry, but due time limitations, we have to make the final competition a little faster. So basically I reduced the timer tick timespan, and I had to change the speed limits:
- default speed limits: -3 .. +3
- extra speed with side tasks: the pre-defined percentage of 3. THUS, max. +190% (max. +5,7)
- extra speed with side tasks: number of solved side tasks * 0,1 . THUS, max. +0,5

Also, internal (non-public) speed and inertia constants had to be changed a little for the final competition to be a little faster. If you have measured distances before, you should re-calculate!

Player_maxspeed is float from now on
