- Side tasks: uploadable until 14:30

- DB delete (matches+games) expected once in every 30 minutes (around 14:30, 15:00, 15:30, 16:00)
- FULL DB delete (matches+games+clients) at 16:30
- After 16:30 only ONE CLIENT PER TEAM is allowed, so do not have more than one record in the Players table!
- Order of teams in the roster is determined by the game stats between 16:00 and 16:30
- DB delete (matches+games) at 16:50, no more db operations allowed

Matches will be pre-recorded to the database, so have your clients ready in the required times!
- 17:00-17:25 Best of 32 (~16 matches simultaneously)
- 17:35-17:55 Best of 16 (8 matches simultaneously)
- 18:05-18:20 Best of 8 (4 consecutive matches, 3 minutes each)
- 18:30-18:50 Best of 4 (2x semi finals, bronze match, finals, 3 minutes each)

Throughout the competition, you can check live results in the view stats_per_match_with_names
